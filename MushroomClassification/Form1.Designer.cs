﻿
namespace MushroomClassification {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btn_predict = new System.Windows.Forms.Button();
            this.btn_startService = new System.Windows.Forms.Button();
            this.cb_capShape = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_capSurface = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_capColor = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_bruises = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cb_odor = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_gillAttachment = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_StalkSurAbove = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cb_stalkRoot = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cb_stalkShape = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cb_gillColor = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cb_gillSize = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_gillSpacing = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cb_ringNumber = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cb_veilColor = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cb_veilType = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cb_stalkColBelow = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cb_stalkColAbove = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.cb_stalkSurBelow = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cb_habitat = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cb_population = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cb_sporePintCol = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cb_ringType = new System.Windows.Forms.ComboBox();
            this.btn_errors = new System.Windows.Forms.Button();
            this.pb_isEdibleBar = new System.Windows.Forms.ProgressBar();
            this.pb_notEdibleBar = new System.Windows.Forms.ProgressBar();
            this.lbl_edible = new System.Windows.Forms.Label();
            this.lbl_notEdible = new System.Windows.Forms.Label();
            this.lbl_yesNoEdible = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btn_predict
            // 
            this.btn_predict.Location = new System.Drawing.Point(657, 415);
            this.btn_predict.Name = "btn_predict";
            this.btn_predict.Size = new System.Drawing.Size(131, 23);
            this.btn_predict.TabIndex = 1;
            this.btn_predict.Text = "Predict";
            this.btn_predict.UseVisualStyleBackColor = true;
            this.btn_predict.Click += new System.EventHandler(this.btn_predict_Click);
            // 
            // btn_startService
            // 
            this.btn_startService.Location = new System.Drawing.Point(657, 386);
            this.btn_startService.Name = "btn_startService";
            this.btn_startService.Size = new System.Drawing.Size(131, 23);
            this.btn_startService.TabIndex = 3;
            this.btn_startService.Text = "Start Service";
            this.btn_startService.UseVisualStyleBackColor = true;
            this.btn_startService.Click += new System.EventHandler(this.btn_startService_Click);
            // 
            // cb_capShape
            // 
            this.cb_capShape.FormattingEnabled = true;
            this.cb_capShape.Location = new System.Drawing.Point(12, 29);
            this.cb_capShape.Name = "cb_capShape";
            this.cb_capShape.Size = new System.Drawing.Size(121, 21);
            this.cb_capShape.TabIndex = 4;
            this.cb_capShape.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "cap-shape";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "cap-surface";
            // 
            // cb_capSurface
            // 
            this.cb_capSurface.FormattingEnabled = true;
            this.cb_capSurface.Location = new System.Drawing.Point(12, 75);
            this.cb_capSurface.Name = "cb_capSurface";
            this.cb_capSurface.Size = new System.Drawing.Size(121, 21);
            this.cb_capSurface.TabIndex = 6;
            this.cb_capSurface.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "cap-color";
            // 
            // cb_capColor
            // 
            this.cb_capColor.FormattingEnabled = true;
            this.cb_capColor.Location = new System.Drawing.Point(12, 125);
            this.cb_capColor.Name = "cb_capColor";
            this.cb_capColor.Size = new System.Drawing.Size(121, 21);
            this.cb_capColor.TabIndex = 8;
            this.cb_capColor.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "bruises";
            // 
            // cb_bruises
            // 
            this.cb_bruises.FormattingEnabled = true;
            this.cb_bruises.Location = new System.Drawing.Point(12, 177);
            this.cb_bruises.Name = "cb_bruises";
            this.cb_bruises.Size = new System.Drawing.Size(121, 21);
            this.cb_bruises.TabIndex = 10;
            this.cb_bruises.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "odor";
            // 
            // cb_odor
            // 
            this.cb_odor.FormattingEnabled = true;
            this.cb_odor.Location = new System.Drawing.Point(12, 230);
            this.cb_odor.Name = "cb_odor";
            this.cb_odor.Size = new System.Drawing.Size(121, 21);
            this.cb_odor.TabIndex = 12;
            this.cb_odor.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "gill-attachment";
            // 
            // cb_gillAttachment
            // 
            this.cb_gillAttachment.FormattingEnabled = true;
            this.cb_gillAttachment.Location = new System.Drawing.Point(12, 283);
            this.cb_gillAttachment.Name = "cb_gillAttachment";
            this.cb_gillAttachment.Size = new System.Drawing.Size(121, 21);
            this.cb_gillAttachment.TabIndex = 14;
            this.cb_gillAttachment.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(227, 267);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "stalk-surface-above-ring";
            // 
            // cb_StalkSurAbove
            // 
            this.cb_StalkSurAbove.FormattingEnabled = true;
            this.cb_StalkSurAbove.Location = new System.Drawing.Point(226, 283);
            this.cb_StalkSurAbove.Name = "cb_StalkSurAbove";
            this.cb_StalkSurAbove.Size = new System.Drawing.Size(121, 21);
            this.cb_StalkSurAbove.TabIndex = 26;
            this.cb_StalkSurAbove.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(227, 214);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "stalk-root";
            // 
            // cb_stalkRoot
            // 
            this.cb_stalkRoot.FormattingEnabled = true;
            this.cb_stalkRoot.Location = new System.Drawing.Point(226, 230);
            this.cb_stalkRoot.Name = "cb_stalkRoot";
            this.cb_stalkRoot.Size = new System.Drawing.Size(121, 21);
            this.cb_stalkRoot.TabIndex = 24;
            this.cb_stalkRoot.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(227, 161);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "stalk-shape";
            // 
            // cb_stalkShape
            // 
            this.cb_stalkShape.FormattingEnabled = true;
            this.cb_stalkShape.Location = new System.Drawing.Point(226, 177);
            this.cb_stalkShape.Name = "cb_stalkShape";
            this.cb_stalkShape.Size = new System.Drawing.Size(121, 21);
            this.cb_stalkShape.TabIndex = 22;
            this.cb_stalkShape.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(227, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "gill-color";
            // 
            // cb_gillColor
            // 
            this.cb_gillColor.FormattingEnabled = true;
            this.cb_gillColor.Location = new System.Drawing.Point(226, 125);
            this.cb_gillColor.Name = "cb_gillColor";
            this.cb_gillColor.Size = new System.Drawing.Size(121, 21);
            this.cb_gillColor.TabIndex = 20;
            this.cb_gillColor.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(227, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "gill-size";
            // 
            // cb_gillSize
            // 
            this.cb_gillSize.FormattingEnabled = true;
            this.cb_gillSize.Location = new System.Drawing.Point(226, 75);
            this.cb_gillSize.Name = "cb_gillSize";
            this.cb_gillSize.Size = new System.Drawing.Size(121, 21);
            this.cb_gillSize.TabIndex = 18;
            this.cb_gillSize.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(227, 13);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "gill-spacing";
            // 
            // cb_gillSpacing
            // 
            this.cb_gillSpacing.FormattingEnabled = true;
            this.cb_gillSpacing.Location = new System.Drawing.Point(226, 29);
            this.cb_gillSpacing.Name = "cb_gillSpacing";
            this.cb_gillSpacing.Size = new System.Drawing.Size(121, 21);
            this.cb_gillSpacing.TabIndex = 16;
            this.cb_gillSpacing.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(429, 267);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "ring-number";
            // 
            // cb_ringNumber
            // 
            this.cb_ringNumber.FormattingEnabled = true;
            this.cb_ringNumber.Location = new System.Drawing.Point(428, 283);
            this.cb_ringNumber.Name = "cb_ringNumber";
            this.cb_ringNumber.Size = new System.Drawing.Size(121, 21);
            this.cb_ringNumber.TabIndex = 38;
            this.cb_ringNumber.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(429, 214);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "veil-color";
            // 
            // cb_veilColor
            // 
            this.cb_veilColor.FormattingEnabled = true;
            this.cb_veilColor.Location = new System.Drawing.Point(428, 230);
            this.cb_veilColor.Name = "cb_veilColor";
            this.cb_veilColor.Size = new System.Drawing.Size(121, 21);
            this.cb_veilColor.TabIndex = 36;
            this.cb_veilColor.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(429, 161);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "veil-type";
            // 
            // cb_veilType
            // 
            this.cb_veilType.FormattingEnabled = true;
            this.cb_veilType.Location = new System.Drawing.Point(428, 177);
            this.cb_veilType.Name = "cb_veilType";
            this.cb_veilType.Size = new System.Drawing.Size(121, 21);
            this.cb_veilType.TabIndex = 34;
            this.cb_veilType.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(429, 109);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = "stalk-color-below-ring";
            // 
            // cb_stalkColBelow
            // 
            this.cb_stalkColBelow.FormattingEnabled = true;
            this.cb_stalkColBelow.Location = new System.Drawing.Point(428, 125);
            this.cb_stalkColBelow.Name = "cb_stalkColBelow";
            this.cb_stalkColBelow.Size = new System.Drawing.Size(121, 21);
            this.cb_stalkColBelow.TabIndex = 32;
            this.cb_stalkColBelow.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(429, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "stalk-color-above-ring";
            // 
            // cb_stalkColAbove
            // 
            this.cb_stalkColAbove.FormattingEnabled = true;
            this.cb_stalkColAbove.Location = new System.Drawing.Point(428, 75);
            this.cb_stalkColAbove.Name = "cb_stalkColAbove";
            this.cb_stalkColAbove.Size = new System.Drawing.Size(121, 21);
            this.cb_stalkColAbove.TabIndex = 30;
            this.cb_stalkColAbove.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(429, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(118, 13);
            this.label18.TabIndex = 29;
            this.label18.Text = "stalk-surface-below-ring";
            // 
            // cb_stalkSurBelow
            // 
            this.cb_stalkSurBelow.FormattingEnabled = true;
            this.cb_stalkSurBelow.Location = new System.Drawing.Point(428, 29);
            this.cb_stalkSurBelow.Name = "cb_stalkSurBelow";
            this.cb_stalkSurBelow.Size = new System.Drawing.Size(121, 21);
            this.cb_stalkSurBelow.TabIndex = 28;
            this.cb_stalkSurBelow.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(626, 161);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 47;
            this.label21.Text = "habitat";
            // 
            // cb_habitat
            // 
            this.cb_habitat.FormattingEnabled = true;
            this.cb_habitat.Location = new System.Drawing.Point(625, 177);
            this.cb_habitat.Name = "cb_habitat";
            this.cb_habitat.Size = new System.Drawing.Size(121, 21);
            this.cb_habitat.TabIndex = 46;
            this.cb_habitat.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(626, 109);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 13);
            this.label22.TabIndex = 45;
            this.label22.Text = "population";
            // 
            // cb_population
            // 
            this.cb_population.FormattingEnabled = true;
            this.cb_population.Location = new System.Drawing.Point(625, 125);
            this.cb_population.Name = "cb_population";
            this.cb_population.Size = new System.Drawing.Size(121, 21);
            this.cb_population.TabIndex = 44;
            this.cb_population.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(626, 59);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 13);
            this.label23.TabIndex = 43;
            this.label23.Text = "spore-print-color";
            // 
            // cb_sporePintCol
            // 
            this.cb_sporePintCol.FormattingEnabled = true;
            this.cb_sporePintCol.Location = new System.Drawing.Point(625, 75);
            this.cb_sporePintCol.Name = "cb_sporePintCol";
            this.cb_sporePintCol.Size = new System.Drawing.Size(121, 21);
            this.cb_sporePintCol.TabIndex = 42;
            this.cb_sporePintCol.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(626, 13);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 13);
            this.label24.TabIndex = 41;
            this.label24.Text = "ring-type";
            // 
            // cb_ringType
            // 
            this.cb_ringType.FormattingEnabled = true;
            this.cb_ringType.Location = new System.Drawing.Point(625, 29);
            this.cb_ringType.Name = "cb_ringType";
            this.cb_ringType.Size = new System.Drawing.Size(121, 21);
            this.cb_ringType.TabIndex = 40;
            this.cb_ringType.Click += new System.EventHandler(this.dropDownElementClicked);
            // 
            // btn_errors
            // 
            this.btn_errors.Location = new System.Drawing.Point(657, 357);
            this.btn_errors.Name = "btn_errors";
            this.btn_errors.Size = new System.Drawing.Size(131, 23);
            this.btn_errors.TabIndex = 48;
            this.btn_errors.Text = "Errors?";
            this.btn_errors.UseVisualStyleBackColor = true;
            // 
            // pb_isEdibleBar
            // 
            this.pb_isEdibleBar.Location = new System.Drawing.Point(16, 386);
            this.pb_isEdibleBar.Name = "pb_isEdibleBar";
            this.pb_isEdibleBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pb_isEdibleBar.Size = new System.Drawing.Size(285, 23);
            this.pb_isEdibleBar.TabIndex = 49;
            // 
            // pb_notEdibleBar
            // 
            this.pb_notEdibleBar.Location = new System.Drawing.Point(307, 386);
            this.pb_notEdibleBar.Name = "pb_notEdibleBar";
            this.pb_notEdibleBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pb_notEdibleBar.Size = new System.Drawing.Size(285, 23);
            this.pb_notEdibleBar.TabIndex = 50;
            // 
            // lbl_edible
            // 
            this.lbl_edible.AutoSize = true;
            this.lbl_edible.Location = new System.Drawing.Point(16, 367);
            this.lbl_edible.Name = "lbl_edible";
            this.lbl_edible.Size = new System.Drawing.Size(36, 13);
            this.lbl_edible.TabIndex = 51;
            this.lbl_edible.Text = "Edible";
            // 
            // lbl_notEdible
            // 
            this.lbl_notEdible.AutoSize = true;
            this.lbl_notEdible.Location = new System.Drawing.Point(537, 367);
            this.lbl_notEdible.Name = "lbl_notEdible";
            this.lbl_notEdible.Size = new System.Drawing.Size(56, 13);
            this.lbl_notEdible.TabIndex = 52;
            this.lbl_notEdible.Text = "Not Edible";
            // 
            // lbl_yesNoEdible
            // 
            this.lbl_yesNoEdible.AutoSize = true;
            this.lbl_yesNoEdible.Location = new System.Drawing.Point(279, 367);
            this.lbl_yesNoEdible.Name = "lbl_yesNoEdible";
            this.lbl_yesNoEdible.Size = new System.Drawing.Size(49, 13);
            this.lbl_yesNoEdible.TabIndex = 53;
            this.lbl_yesNoEdible.Text = "Not Sure";
            // 
            // panel1
            // 
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(307, 415);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(286, 23);
            this.panel1.TabIndex = 54;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbl_yesNoEdible);
            this.Controls.Add(this.lbl_notEdible);
            this.Controls.Add(this.lbl_edible);
            this.Controls.Add(this.pb_notEdibleBar);
            this.Controls.Add(this.pb_isEdibleBar);
            this.Controls.Add(this.btn_errors);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.cb_habitat);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.cb_population);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.cb_sporePintCol);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.cb_ringType);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cb_ringNumber);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cb_veilColor);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cb_veilType);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.cb_stalkColBelow);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cb_stalkColAbove);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.cb_stalkSurBelow);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cb_StalkSurAbove);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cb_stalkRoot);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cb_stalkShape);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cb_gillColor);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cb_gillSize);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cb_gillSpacing);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cb_gillAttachment);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cb_odor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cb_bruises);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cb_capColor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cb_capSurface);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_capShape);
            this.Controls.Add(this.btn_startService);
            this.Controls.Add(this.btn_predict);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Mushroom Classificator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_predict;
        private System.Windows.Forms.Button btn_startService;
        private System.Windows.Forms.ComboBox cb_capShape;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_capSurface;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_capColor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_bruises;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cb_odor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cb_gillAttachment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cb_StalkSurAbove;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cb_stalkRoot;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cb_stalkShape;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cb_gillColor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cb_gillSize;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cb_gillSpacing;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cb_ringNumber;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cb_veilColor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cb_veilType;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cb_stalkColBelow;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cb_stalkColAbove;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cb_stalkSurBelow;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cb_habitat;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cb_population;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cb_sporePintCol;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cb_ringType;
        private System.Windows.Forms.Button btn_errors;
        private System.Windows.Forms.ProgressBar pb_isEdibleBar;
        private System.Windows.Forms.ProgressBar pb_notEdibleBar;
        private System.Windows.Forms.Label lbl_edible;
        private System.Windows.Forms.Label lbl_notEdible;
        private System.Windows.Forms.Label lbl_yesNoEdible;
        private System.Windows.Forms.Panel panel1;
    }
}

