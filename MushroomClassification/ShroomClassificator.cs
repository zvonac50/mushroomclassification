﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MushroomClassification {

    class ShroomClassificator {
        private static string[,] shroomParameters;
        private static string[] shroomPredictionResults;
        private static string shroomPredictionResultsError;

        public void setParameters(string[,] mShroomParameters) {
            shroomParameters = mShroomParameters;
        }

        public string[] getResults() {
            return shroomPredictionResults;
        }
        public string getResultsError() {
            return shroomPredictionResultsError;
        }

        public void StartPredictionService() {
            _ = InvokeRequestResponseService();
        }

        private static async Task InvokeRequestResponseService() {
            using (var client = new HttpClient()) {
                var scoreRequest = new {

                    Inputs = new Dictionary<string, StringTable>() {
                        {
                            "input1",
                            new StringTable()
                            {
                                ColumnNames = new string[] {"class", "cap-shape", "cap-surface", "cap-color", "bruises", "odor", "gill-attachment", "gill-spacing", "gill-size", "gill-color", "stalk-shape", "stalk-root", "stalk-surface-above-ring", "stalk-surface-below-ring", "stalk-color-above-ring", "stalk-color-below-ring", "veil-type", "veil-color", "ring-number", "ring-type", "spore-print-color", "population", "habitat"},
                                Values = shroomParameters
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>() {
                    }
                };
                const string apiKey = "/BITnm7AxUeR8RlP3cQmnIppKySnCWC0XjpKfjqrvlm+jyCDJzJw2E12Qu35FeBerJQN1pSfTtJr4Udga2NMlA==";
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress = new Uri("https://ussouthcentral.services.azureml.net/workspaces/a88b4865bfbb40849c67229aed213f90/services/2953723af2584d268e44cab4207f23ea/execute?api-version=2.0&details=true");

                // WARNING: The 'await' statement below can result in a deadlock if you are calling this code from the UI thread of an ASP.Net application.
                // One way to address this would be to call ConfigureAwait(false) so that the execution does not attempt to resume on the original context.
                // For instance, replace code such as:
                //      result = await DoSomeTask()
                // with the following:
                //      result = await DoSomeTask().ConfigureAwait(false)


                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);

                if (response.IsSuccessStatusCode) {
                    string result = await response.Content.ReadAsStringAsync();
                    shroomPredictionResults = customJsonConvert(result);

                    shroomPredictionResultsError = null;
                } else {
                    shroomPredictionResultsError = response.StatusCode.ToString();

                    // Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
                    shroomPredictionResultsError = shroomPredictionResultsError + "\n" + response.Headers.ToString();

                    string responseContent = await response.Content.ReadAsStringAsync();
                    shroomPredictionResultsError = shroomPredictionResultsError + "\n" + responseContent;

                    shroomPredictionResults = null;
                }
            }
        }

        private static string[] customJsonConvert(string jsonFile) {
            string[] result = new string[2];

            for (int i = 0; i < jsonFile.Length; i++) {
                if (jsonFile[i] == '$') {
                    result[0] += jsonFile[i + 4];

                    for (int j = 0; j < 7; j++) {
                        result[1] += jsonFile[i + 8 + j];
                    }
                }
            }

            return result;
        }
    }
}